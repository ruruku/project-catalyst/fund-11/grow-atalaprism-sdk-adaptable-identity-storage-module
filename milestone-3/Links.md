# Milestone Evidence Links

## Repository Issues
1. https://github.com/hyperledger/identus-edge-agent-sdk-ts/issues
2. https://github.com/atala-community-projects/pluto-encrypted/issues

## Updated Documentation
### SDK Docs

1. https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/250 
2. https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/252
3. https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/259

### Pluto-Encrypted Docs
1. https://github.com/atala-community-projects/pluto-encrypted/pull/99

## New Ahau release
1. https://github.com/Ahau-NZ/Ahau/releases

## Video link
1. https://drive.google.com/file/d/1nXLwHLcemBya_djjwSjDfCDbOahAH5WQ/view?usp=sharing

## Presentation link
1. https://docs.google.com/presentation/d/1QQT_fKIk-5NgTPhpc_HkAHiM6zyxgtsPKVkreiugpTk/edit?usp=sharing