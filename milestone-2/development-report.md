# Development Update Report

## Improving SDK accessibility

Our work helped improve the the SDK:

- our collaboration with @elribonazo produced the "pluto-encrypted" project
- validated a plugin system which makes it much easier for users to implement storage!
    - `@pluto-encrypted/database` forms the core
        - manages data models, migrations etc
        - takes user-built plugins
    - `@pluto-encrypted/leveldb` is the plugin we made for Ahau (based on our needs)

This is working and in production in Ahau currently

## SDK Evolution

On the basis of our work, the Atala/Identus team decided to integrate the `@pluto-encrypted/database` back into the SDK:

- `@pluto-encrypted/database` was integrated into the SDK ([see PR](https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/160))
- users no longer have to implement all of `Pluto`, only the minimal `Pluto.Store` (collection of read/write functions)
- the underlying `rxdb` was upgraded a major version

## Currently

Currently `@pluto-encrypted/leveldb` works in production.

As part of this grant we've decided to futher improve "pluto-encrypted" documentation and testing to make it easier for others to implement their own storage.

We're upgrading the packages to respond to the major version changes in the SDK (and `rxdb`)

## Pull requests

**SDK PRs**

- [link to community pluto.store work](https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/250)

**pluto-encrypted PRs**

- [leveldb module - upgrade](https://github.com/atala-community-projects/pluto-encrypted/pull/102) [DRAFT]
- [database module removed](https://github.com/atala-community-projects/pluto-encrypted/pull/98)
    - the database package was absorbed into `@atala/prism-wallet-sdk` module!
        - this validates our work
    - this was merged [here](https://github.com/atala-community-projects/pluto-encrypted/pull/100)
- [update README](https://github.com/atala-community-projects/pluto-encrypted/pull/99)
    - show how these modules are used with `@atala/prism-wallet-sdk`
- improve testing
    - [replace jest => vitest](https://github.com/atala-community-projects/pluto-encrypted/pull/43) - more stable
    - [output readability](https://github.com/atala-community-projects/pluto-encrypted/pull/44)
    - [leveldb multi-instance](https://github.com/atala-community-projects/pluto-encrypted/pull/62)
- [leveldb module](https://github.com/atala-community-projects/pluto-encrypted/pull/41)
    - << delivered!
    - installed in [Ahau](https://www.notion.so/matou-collective/www.ahau.io) by [ssb-atala-prism](https://gitlab.com/ahau/lib/ssb-plugins/ssb-atala-prism) (plugin which adds AtalaPrism / Identus functionality to scuttlebutt)