# Architecture

The architecture consists of several key components:

1. **Āhau**: A decentralised data management app, which also integrates the wallet into its Tribal registration process, allowing the creation, management and verification of digital identities by Tribal Members and Tribal Kaitiaki.
2. **Pātaka**: An Āhau tool which allows Āhau users to sync databases with each other when online.
3. **Atala Prism Wallet SDK Agents (Holders):** A tool which enables developers to integrate decentralised identity management capabilities into their applications and services. This module is also responsible for initiating connection requests with cloud agents on behalf of holders.
4. **Issuer Agents (Cloud Agent)**: A cloud agent that is responsible for handling connection invitations and digital identity management for wallet SDK agents, particularly with the issuance of credentials and handling issuance requests.
5. **Verifier Agents (Cloud Agent):** A cloud agent that is responsible for handling connection invitations and digital identity verification for wallet SDK agents, particularly with the verification of credentials and handling presentation requests.
6. **Mediator**: Responsible for the secure communication and interaction between various entities such as holders and cloud agents.
7. **Pluto-Encrypted:** The wallet identity storage module that is reponsible for storing DID's, Credentials and Connections. This module will be the focus of our work

## **Key Features**

1. **Data storage by Holders:** Prism Wallet Agents (Holders) can now store DIDs, messages and credentials, significantly expanding the agents features, enabling smoother integration with decentralised identity frameworks.
2. **Use Case Demonstration:** Āhau integration provides a practical demonstration of the SDK’s enhanced functionalities, enabling seamless integration for developers.
3. **Testing and Quality Assurance:** Rigorous testing ensures that the system meets performance, security, and reliability requirements.

## **High-Level Architecture Diagrams**

![System Architecture](../assets/system-architecture-1.png)
![System Architecture](../assets/system-architecture-2.png)